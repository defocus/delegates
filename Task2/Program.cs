﻿using System;

namespace Task2
{
    delegate double Values();

    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter number of values\n\t>> ");

            int length = int.Parse(Console.ReadLine());

            Values[] averages = FillArrayDelegates(length);

            //Console.WriteLine(new string('-', 20));

            //double averageSum = CalculateAverageArrayDelegates(averages);

            //Console.WriteLine(new string('-', 20));

            //Console.WriteLine($"Average: {averageSum}");

            Values average = () =>
            {
                double sum = 0, temp;

                foreach (var item in averages)
                {
                    temp = item();

                    sum += temp;

                    Console.WriteLine($"{temp}");
                }

                return sum / (double)averages.Length;
            };

            Console.WriteLine(new string('-', 20));

            Console.WriteLine($"\nAverage: {average()}");
        }

        static private double CalculateAverageArrayDelegates(Values[] averages)
        {
            double sum = 0, temp;

            foreach (var item in averages)
            {
                temp = item();

                sum += temp;

                Console.WriteLine($"{temp}");
            }

            return sum / (double)averages.Length;
        }

        static private Values[] FillArrayDelegates(int length)
        {
            Values[] averages = new Values[length];

            for (int i = 0; i < averages.Length; i++)
                averages[i] = () => new Random().Next(-1000, 1001);

            return averages;
        }
    }
}
