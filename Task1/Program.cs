﻿using System;
using System.Collections.Generic;

namespace Task1
{
    delegate double Calculator(double x, double y);
    
    internal class Program
    {
        static void Main(string[] args)
        {
            Calculator Add = (x,y) => x + y;
            
            Calculator Sub = (x,y) => x - y;
            
            Calculator Mul = (x,y) => x * y;
            
            Calculator Div = (x, y) =>
                {
                    if (y != 0) return x / y;
                    else
                    {
                        Console.WriteLine("Entered not available number. Not divided by zero.");
                    
                        return 0;
                    }
                };

            double num1 = 0, num2 = 0;

            EnteredNum(ref num1, ref num2);

            Console.Write("Choose operation:\n" +
                "\t+\n" +
                "\t-\n" +
                "\t*\n" +
                "\t/\n" +
                "   >> ");

            string operation = Console.ReadLine();

            Console.Write($"Result {num1} {operation} {num2} = ");

            Console.Write(operation switch
                    {
                        "+" => Add(num1,num2),
                        "-" => Sub(num1,num2),
                        "*" => Mul(num1,num2),
                        "/" => Div(num1, num2),
                        _ => "Didn't choose operation"
                    });

            Console.ReadKey();            
        }

        private static void EnteredNum(ref double x, ref double y)
        {
            Console.Write("Enter number 1: ");

            x = double.Parse(Console.ReadLine());

            Console.Write("Enter number 2: ");

            y = double.Parse(Console.ReadLine());
        }

        
    }
}
